


Vue.use(Vuesax)

let vm=new Vue({
    el:"#app",
    data:{
        name:"",
        contact:{
            id:"",
            nom:"",
            prenom:"",
            adresse:"",
            photo:"https://i.pravatar.cc/300",
            tel:""
        },
        active:false,
        searching:false,
        openeditor:false,
        modal:false,
        contacts:[],
        searchresults:[],
        store:[]
    },
    delimiters: ['${', '}'],
    methods:{
        editor:function(id){
            vm.contact=getContact(id);
            vm.openeditor=!vm.openeditor;

        },
        search:function(){
            console.log("search triggered")
            let clone=JSON.parse(JSON.stringify(vm.store));
           if(vm.name.length > 0){
               vm.searching=true;
               vm.searchresults=filtreNom(clone,vm.name);
           }else{
               vm.searching=false;
           }
        },
        select:function(){
            let input=document.getElementById('image');
            input.click();
        },
        preview:function(e){
            let prev=document.getElementById('preview');
            let comp=e.target;
            var selectedFiles = comp.files;
            let reader = new FileReader();
            reader.addEventListener('load', function(){
                vm.contact.photo=reader.result;
                prev.src=vm.contact.photo;
            }.bind(this), false);
            reader.readAsDataURL(selectedFiles[0]);
        },
        add:function(){
            vm.contact={};
            vm.active=!vm.active;
        },
        del:function(id){
            vm.contact=getContact(id);
            let xhr=new XMLHttpRequest();
            xhr.open("GET","/delete/"+id);
            xhr.onreadystatechange=function(){
                if(xhr.readyState==4){
                    if(xhr.status==200){
                        let response=JSON.parse(xhr.responseText);
                        vm.contacts=response;
                    }
                }
            }
            xhr.send();
        },
        submit:function(){
            let xhr=new XMLHttpRequest();
            let data=new FormData();
            for(let index in vm.contact){
                data.append("_"+index,vm.contact[index])
            }
            xhr.open("POST","/add");
            xhr.onreadystatechange=function(){
                if(xhr.readyState==4){
                    if(xhr.status==200){
                        let response=JSON.parse(xhr.responseText);
                        vm.store=response;
                        vm.contacts=JSON.parse(JSON.stringify(vm.store));
                    }
                }
            }
            xhr.send(data);
            vm.active=!vm.active;
        },
        edit:function(){
            let xhr=new XMLHttpRequest();
            let data=new FormData();
            for(let index in vm.contact){
                data.append("_"+index,vm.contact[index])
            }
            xhr.open("POST","/edit");
            xhr.onreadystatechange=function(){
                if(xhr.readyState==4){
                    if(xhr.status==200){
                        let response=JSON.parse(xhr.responseText);
                        vm.store=response;
                        vm.contacts=JSON.parse(JSON.stringify(vm.store));
                    }
                }
            }
            xhr.send(data);
            vm.openeditor=!vm.openeditor;
        }
    },
    mounted:function(){
        let xhr=new XMLHttpRequest();
        xhr.open("GET","/contacts");
        xhr.onreadystatechange=function(){
            if(xhr.readyState==4){
                if(xhr.status==200){
                    let response=JSON.parse(xhr.responseText);
                    vm.store=response;
                    vm.contacts=JSON.parse(JSON.stringify(vm.store));
                }
            }
        }
        xhr.send();
        let body=document.querySelector('body');
        body.style.display="block";
    },
    beforeUpdate:function () {
        vm.search();

    }

});

function getContact(id){
    for(let i in vm.contacts){
        if(vm.contacts[i].id==id){
            return vm.contacts[i]
        }
    }
}
function filtreNom(arr, requete) {
    console.log(arr);
    let req=requete.toLowerCase();
    let result = arr.filter(function (el) {
        let nom=el.nom.toLowerCase();
        let prenom=el.prenom.toLowerCase();
        return nom.indexOf(req) !== -1 || prenom.indexOf(req) !== -1;
    });
    for(let el of result){
        let nom=el.nom.toLowerCase();
        let prenom=el.prenom.toLowerCase();
        if(nom.indexOf(req)==-1){
            el.nom=el.nom+" "+el.prenom.substring(0,prenom.indexOf(req))+"<b class='matched'>"+el.prenom.substring(prenom.indexOf(req),prenom.indexOf(req)+req.length)+"</b>"+el.prenom.substring(prenom.indexOf(req)+req.length,prenom.length)
        }else{
            el.nom=el.nom.substring(0,nom.indexOf(req))+"<b class='matched'>"+el.nom.substring(nom.indexOf(req),nom.indexOf(req)+req.length)+"</b>"+el.nom.substring(nom.indexOf(req)+req.length,nom.length);
        }
    }
    return result;
}