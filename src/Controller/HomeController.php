<?php
/**
 * Created by PhpStorm.
 * User: manug
 * Date: 12/05/21
 * Time: 22:56
 */

namespace App\Controller;


use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class HomeController extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var ObjectManager
     */
    private $om;
    /**
     * @var ContactRepository
     */
    private $contacts;

    public function __construct(Environment $twig, EntityManagerInterface $om, ContactRepository $contacts)
    {
        $this->twig = $twig;
        $this->om = $om;
        $this->contacts = $contacts;
    }

    public function index():Response{
        $form=$this->createForm(ContactType::class,new Contact());
        $contacts=$this->contacts->findAll();
        return new Response(
            $this->twig->render('index.twig',
                [
                    "contacts"=>$contacts,
                    "form"=>$form->createView()
                ])
        );
    }

    public function add(Request $request):Response{
        $contact=new Contact();
        $contact->setNom($request->request->get("_nom"))
            ->setAdresse($request->request->get("_adresse"))
            ->setPrenom($request->request->get("_prenom"))
            ->setPhoto($request->request->get("_photo"))//will parse it into file later
            ->setTel($request->request->get("_tel"));
        $this->om->persist($contact);
        $this->om->flush();
        $list=$this->contacts->findAll();
        $sr=$this->container->get('serializer');
        $contacts=$sr->serialize($list,'json');
        return new Response($contacts);
        //return new Response(json_encode(["error"=>"Something went wrong"]));
    }
    public function list():Response{
        $list=$this->contacts->findAll();
        $sr=$this->container->get('serializer');
        $contacts=$sr->serialize($list,'json');
        return new Response($contacts);
    }
    public function edit(Request $request){
        $contact=$this->contacts->find($request->request->get("_id"));
        $contact->setNom($request->request->get("_nom"))
            ->setAdresse($request->request->get("_adresse"))
            ->setPrenom($request->request->get("_prenom"))
            ->setPhoto($request->request->get("_photo"))//will parse it into file later
            ->setTel($request->request->get("_tel"));
        $this->om->persist($contact);
        $this->om->flush();
        $list=$this->contacts->findAll();
        $sr=$this->container->get('serializer');
        $contacts=$sr->serialize($list,'json');
        return new Response($contacts);
    }
    public function delete($id):Response{
        $contact=$this->contacts->find($id);
        $this->om->remove($contact);
        $this->om->flush();
        $list=$this->contacts->findAll();
        $sr=$this->container->get('serializer');
        $contacts=$sr->serialize($list,'json');
        return new Response($contacts);
    }
}